#!/bin/bash

set -eu

readonly yaml="/app/code/node_modules/.bin/yaml"

if [[ ! -d /app/data/wiki ]]; then
    echo "Initializing wiki"

    mkdir -p /app/data/wiki
    cd /app/data/wiki
    git init
    git config user.name "Cloudron App"
    git config user.email "support@cloudron.io"
    touch .gitignore && git add .gitignore && git commit -a -m 'Initialize empty repository'
fi

echo "Generating config file"
mkdir -p /run/jingo
sed -e "s,##LDAP_URL,${CLOUDRON_LDAP_URL}," \
    -e "s/##LDAP_BIND_DN/${CLOUDRON_LDAP_BIND_DN}/" \
    -e "s/##LDAP_BIND_PASSWORD/${CLOUDRON_LDAP_BIND_PASSWORD}/" \
    -e "s/##LDAP_USERS_BASE_DN/${CLOUDRON_LDAP_USERS_BASE_DN}/" \
    /app/pkg/config.yaml.template > /run/jingo/config.yaml

if [[ ! -f /app/data/config.yaml ]]; then
    echo "# Add additional customizations in this file" > /app/data/config.yaml
fi

chown -R cloudron:cloudron /app/data /run/jingo

# merge user yaml file (yaml does not allow key re-declaration)
/usr/local/bin/gosu cloudron:cloudron node /app/pkg/yaml-override.js /run/jingo/config.yaml /app/data/config.yaml

echo "Starting jingo"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/jingo --config /run/jingo/config.yaml

