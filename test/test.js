#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);
    const LOCATION = 'test';
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 5000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.startsWith(LOCATION); })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can get the main page', async function () {
        const response = await superagent.get('https://' + app.fqdn);
        expect(response.status).to.eql(200);
    });

    async function login() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Login"]')).click();
        await browser.sleep(3000);
    }

    async function edit() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(async function () {
            const url = await browser.getCurrentUrl();
            return url === 'https://' + app.fqdn + '/pages/new/Home';
        }, TIMEOUT);
        // code mirror uses some hidden textarea
        const cm = browser.findElement(By.xpath('//div[contains(@class,"CodeMirror")]'));
        const text = 'hello jingo';
        await browser.executeScript('arguments[0].CodeMirror.setValue("' + text + '");', cm);
        await browser.findElement(By.xpath('//input[@value="Save"]')).submit();
        await browser.wait(async function () {
            const url = await browser.getCurrentUrl();
            return url === 'https://' + app.fqdn + '/wiki/Home';
        }, TIMEOUT);
    }

    async function read() {
        await browser.get('https://' + app.fqdn);
        const text = 'hello jingo';
        await browser.findElement(By.xpath('//*[contains(text(), "' + text + '")]'));
    }

    it('can login', login);
    it('can edit', edit);
    it('results in 404 for non-existent file', async function () {
        await browser.get('https://' + app.fqdn + '/unknown');
        await browser.findElement(By.xpath('//*[contains(text(), "The requested resource is not available")]'));
    });

    it('can restart app', function () {
        execSync('cloudron restart');
    });

    // should not require re-login
    it('can read existing page', read);
    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    // should not require re-login
    it('can read existing page', async function () {
        await browser.get('https://' + app.fqdn);
        const text = 'hello jingo';
        await browser.findElement(By.xpath('//*[contains(text(), "' + text + '")]'));
    });

    it('move to different location', function () {
        execSync('cloudron configure --location ' + LOCATION + '2', EXEC_ARGS);
        getAppInfo();
    });

    it('can login', login);

    it('can read existing page', async function () {
        browser.get('https://' + app.fqdn);
        const text = 'hello jingo';
        await browser.findElement(By.xpath('//*[contains(text(), "' + text + '")]'));
    });

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', async function () {
        execSync('cloudron install --appstore-id li.cica.jingo --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });
    it('can get app information', getAppInfo);
    // it('can login', login); // mystery why this is not required across re-installs. seems some jingo bug
    it('can edit', edit);
    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
    });
    it('can read', read);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
