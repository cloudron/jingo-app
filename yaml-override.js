#!/usr/bin/env node

'use strict';

var fs = require('fs'),
    yaml = require('js-yaml');

var target = yaml.load(fs.readFileSync(process.argv[2] ,'utf8'));
var source = yaml.load(fs.readFileSync(process.argv[3], 'utf8'));
target = Object.assign(target, source);
fs.writeFileSync(process.argv[2], yaml.dump(target));

