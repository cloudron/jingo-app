FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

ARG VERSION=1.9.5

RUN mkdir -p /app/code
WORKDIR /app/code
RUN curl -L https://github.com/claudioc/jingo/archive/v1.9.5.tar.gz | tar -vxz --strip-components 1 -f -
RUN npm install --production && npm install passport-ldapauth@2.0.0

COPY start.sh config.yaml.template yaml-override.js /app/pkg/
WORKDIR /app/pkg
RUN npm install js-yaml

CMD [ "/app/pkg/start.sh" ]

