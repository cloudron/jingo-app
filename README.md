# Jingo Cloudron App

This repository contains the Cloudron app package source for [Jingo](https://github.com/claudioc/jingo).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=li.cica.jingo)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id li.cica.jingo
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd jingo-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the repos are still ok.

```
cd jingo-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

