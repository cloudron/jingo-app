This app packages Jingo <upstream>1.9.5</upstream>.

### Introduction

The aim of this wiki engine is to provide an easy way to create a centralized documentation area for people used to work with **git** and **markdown**. It should fit well into a development team without the burden to have to learn a complex and usually overkill application.

Jingo is very much inspired by (and format-compatible with) the github own wiki system [Gollum](https://github.com/gollum/gollum), but it tries to be more a stand-alone and complete system than Gollum is.

Think of jingo as "the github wiki, without github but with more features". "Jingo" means "Jingo is not Gollum" for more than one reason.

### Accounts

This app provides a private wiki that can only be used by this Cloudron's users.

### Features

- Markdown for everything, [github flavored](http://github.github.com/github-flavored-markdown/)
- Uses [Codemirror](http://codemirror.net/) or [Markitup](http://markitup.jaysalvat.com/home/) as the markup editor, with a nice (ajax) preview (see the `features` key in the config file)
- Provides a "distraction free", almost full screen editing mode
- Compatible with a wiki created with the [Gollum](https://github.com/github/gollum) wiki
- Revision history for all the pages (and restore)
- Show differences between document revisions
- Paginated list of all the pages, with a quick way to find changes between revisions
- Search through the content _and_ the page names
- Page layout accepts custom sidebar and footer
- Gravatar support
- Can include IFRAMEs in the document (es: embed a Google Drive document)
- Can use custom CSS and JavaScript scripts
- White list for authorization on page reading and writing
- Detects unwritten pages (which will appear in red)
- Automatically push to a remote (optionally)
- Mobile friendly (based on Bootstrap 3.x)
- Quite configurable, but also works out of the box
- Works well behind a proxy (i.e.: the wiki can be "mounted" as a directory in another website)
- Pages can be embedded into another site

### Customization

You can customize jingo in four different ways:

- add a left sidebar to every page: just add a file named `_sidebar.md` containing the markdown you want to display to the repository. You can edit or create the sidebar from Jingo itself, visiting `/wiki/_sidebar` (note that the title of the page in this case is useless)
- add a footer to every page: the page you need to create is `_footer.md` and the same rules for the sidebar apply
